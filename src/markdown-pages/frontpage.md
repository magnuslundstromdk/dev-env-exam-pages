---
slug: "/"
title: "About the project"
---

Todo or not todo is a simple web application built in the Python framework called Django. It allows users to create an account, create new todos and display current todos. Also they can mark todos as completed or even entirely delete them

### Running our app with Docker

In order to be able to run a dockerized version of our project on your machine you need to take these steps:

- Clone [this repo](https://gitlab.com/magnuslundstromdk/dev-env-exam)
- Using your terminal, you need to access the cloned project and move into it: “cd /your/folder
- From the same terminal you must run our docker-compose.yml file using the following syntax: “docker-compose up”
- When the docker container is up and running you can open your browser and navigate to localhost:80 where you will be able to see and use the application.
